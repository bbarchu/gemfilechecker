require_relative 'presenter.rb'
class Validator

  
  def process(gemfile_content)
    p = Presenter.new()
    if gemfile_content.empty?
      p.gemfile_incorrecto("vacio")
      return false
    end
    
    if !validar(gemfile_content,"source")
      p.gemfile_incorrecto("sin source")
      return false
    end

    if !validar(gemfile_content, "ruby")
      return false
    end

    if !verificar_gemas_ordenadas(gemfile_content)
      return false
    end
    p.gemfile_correcto()
    return true
  end

  def validar(gemfile_content, string)
    gemfile_content.each_line do |line|
      if line.strip.empty?
        next
      end

      if line.index(string) == 0
        return true
      end
    end
    return false
  end

  def verificar_gemas_ordenadas(gemfile_content)
    gems_names = []
    gemfile_content.each_line do |line|
      if line.strip.empty?
        next
      end
      if line.index("gem") == 0
        first_marker = "'"
        second_marker = "'"
        gem_name = line[/#{first_marker}(.*?)#{second_marker}/m, 1]
        if gems_names.empty?
          gems_names.push gem_name
        end
        if gems_names[-1] > gem_name
          return false
        else 
          gems_names.push gem_name
        end
      end
    end
    return true
  end    
end

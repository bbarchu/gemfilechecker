require 'rspec'
require_relative '../model/validator'

describe Validator do

  subject { @validator = Validator.new }

  it { should respond_to (:process) }

  it 'should validate gemfile string and return boolean' do
    gemfile_string = ''
    result = subject.process gemfile_string
    expect(result).to be false
  end

  it 'should return false if gemfile does not begin with source line' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.source"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return false if gemfile does not have a ruby version' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.ruby"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return true when valid Gemfile' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.valid"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq true
  end

end

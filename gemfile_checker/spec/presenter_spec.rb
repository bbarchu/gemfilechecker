require 'rspec'
require_relative '../model/presenter'

describe Presenter do

  subject { @present = Presenter.new }

  it 'should presenter a correct gemfile' do
    expect do
        Presenter.new.gemfile_correcto
      end.to output("Gemfile correcto\n").to_stdout
  end

  it 'should presenter a incorrect empty gemfile' do
    expect do
        Presenter.new.gemfile_incorrecto("vacio")
      end.to output("Error: Gemfile vacio\n").to_stdout
  end

  it 'should presenter a incorrect without source gemfile' do
    expect do
        Presenter.new.gemfile_incorrecto("sin source")
      end.to output("Error: Gemfile sin source\n").to_stdout
  end

end
